#!/bin/bash
pip3 install "trytond == ${SERIES}.*"                                                 
for module in `curl https://downloads-cdn.tryton.org/${SERIES}/modules.txt`; do
    pip3 install "trytond_${module} == ${SERIES}.*";
done     
pip3 install phonenumbers   
pip3 install "proteus == ${SERIES}.*" 
