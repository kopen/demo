#!/usr/bin/python3
# encoding: utf-8
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
# based on
import datetime
import logging
import random
import os
import string
from collections import defaultdict
from decimal import Decimal
from itertools import chain

from dateutil.relativedelta import relativedelta
from faker import Faker
from faker.providers import bank
from stdnum.es.cif import calc_check_digits
from stdnum.es.dni import calc_check_digit
from proteus import Model, Wizard, config as pconfig
from proteus.config import get_config

from trytond import __version__ as trytond_version
trytond_version = tuple(map(int, trytond_version.split('.')))

__all__ = ['main']

logging.basicConfig(
    format='%(process)s [%(asctime)s] %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

base_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
    'demo_data')

fake = Faker()
fake.add_provider(bank)
Faker.seed(0)


def random_datetime(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + datetime.timedelta(seconds=random_second)


def create_iban(bank_code=None):
    Number = Model.get('bank.account.number')
    while True:
        number = fake.iban()
        exist = Number.find([
            ('number_compact', '=', number)], limit=1)
        if not exist:
            return number


def create_cif(prefix='B'):
    cif = prefix
    while len(cif) < 8:
        cif += str(random.randint(1, 9))
    return 'ES' + cif + calc_check_digits(cif)[0]


created_cifs = set([])


def create_unique_cif(*args, **kwargs):
    cif = create_cif(*args, **kwargs)
    while cif in created_cifs:
        cif = create_cif(*args, **kwargs)
    created_cifs.add(cif)
    return cif


def create_nif(prefix=''):
    nif = prefix
    while len(nif) < 8:
        nif += str(random.randint(1, 9))
    return 'ES' + nif + calc_check_digit(nif)


created_nifs = set([])


def create_unique_nif(*args, **kwargs):
    nif = create_nif(*args, **kwargs)
    while nif in created_nifs:
        nif = create_nif(*args, **kwargs)
    created_nifs.add(nif)
    return nif


def get_phone(prefix=''):
    phone = prefix
    while len(phone) < 9:
        phone += str(random.randrange(9))
    return '+34' + phone


zip_prefix_cache = {}


def get_valid_zip(prefix=''):
    if prefix not in zip_prefix_cache:
        zip_prefix_cache[prefix] = ['%s%s' % (prefix, str(n).rjust(3, '0'))
            for n in range(200)]

    return random.choice(zip_prefix_cache[prefix])


def get_active_modules():
    Module = Model.get('ir.module')
    return set([x.name for x in Module.find([('state', '=', 'activated')])])


def activate_modules(modules=None):
    Module = Model.get('ir.module')
    if modules is None:
        modules = [
            'account_es',
            'account_asset',
            'account_payment_sepa',
            'account_statement_csb43',
            'commission',
            'purchase',
            'production_work',
            'production_work_timesheet',
            'project_revenue',
            'project_invoice',
            'sale',
            'sale_opportunity',
            'sale_price_list',
            'stock_lot',
            'stock_lot_sled',
            'stock_supply_production',
            ]
    to_activate = Module.find([
            ('name', 'in', modules),
            ('state', '!=', 'activated'),
            ])
    if to_activate:
        logger.info('Activating modules %s', ','.join(
                m.name for m in to_activate))
        Module.click(to_activate, 'activate')
        Wizard('ir.module.activate_upgrade').execute('upgrade')


def create_company(config, name='Demo Company, S.L'):
    Company = Model.get('company.company')
    Currency = Model.get('currency.currency')
    Party = Model.get('party.party')

    if not Company.find([]):
        logger.info('Creating Company')
        company_config = Wizard('company.company.config')
        company_config.execute('company')
        company = company_config.form
        party = Party(name='Demo Company, S.L')
        party.identifiers.new(type='eu_vat', code=create_unique_cif('B25'))
        party.save()
        company.party = party
        currency, = Currency.find([('code', '=', 'EUR')])
        company.currency = currency
        company_config.execute('add')

    company, = Company.find(('rec_name', '=', name))
    return company.id


def setup_company(config, company_id, modules):
    Company = Model.get('company.company')
    Bank = Model.get('bank')
    # Rebrowse to get correct context
    company = Company(company_id)
    party = company.party
    if not party.bank_accounts:
        banks = Bank.find([])
        # We must create the values once the context is set
        for _ in range(random.randrange(2, 6)):
            account = party.bank_accounts.new()
            account.bank = random.choice(banks)
            number = account.numbers.new(type='iban', )
            number.number = create_iban()
        party.save()
        if 'account_bank' in modules:
            account = party.bank_accounts[0]
            party.payable_bank_account = account
            party.receivable_bank_account = account
            party.save()

    if 'account' not in modules:
        return

    # Create Chart of accounts for the company
    AccountTemplate = Model.get('account.account.template')
    ModelData = Model.get('ir.model.data')
    Account = Model.get('account.account')

    if 'account_es' in modules:
        data, = ModelData.find([
                ('module', '=', 'account_es'),
                ('fs_id', '=', 'pgc_0_normal'),
                ], limit=1)
    else:
        data, = ModelData.find([
                ('module', '=', 'account'),
                ('fs_id', '=', 'account_template_root_en'),
                ], limit=1)
    template = AccountTemplate(data.db_id)
    if not Account.find([
            ('template', '=', template.id),
            ('company', '=', company.id),
            ]):
        logger.info('Creating chart of Accounts: %s' % template.rec_name)

        create_chart = Wizard('account.create_chart')
        create_chart.execute('account')
        create_chart.form.account_template = template
        create_chart.form.company = company
        if 'account_code_digits' in modules:
            create_chart.form.account_code_digits = 8
        create_chart.execute('create_account')

        receivable_domain = [('company', '=', company.id)]
        if trytond_version >= (5, 1):
            receivable_domain.append(('type.receivable', '=', True))
        else:
            receivable_domain.append(('kind', '=', 'receivable'))
        receivable, = Account.find(receivable_domain, limit=1)
        payable_domain = [('company', '=', company.id)]
        if trytond_version >= (5, 1):
            receivable_domain.append(('type.receivable', '=', True))
            payable_domain.append(('type.payable', '=', True))
        else:
            payable_domain.append(('kind', '=', 'payable'))
        payable, = Account.find(payable_domain, limit=1)

        create_chart.form.account_receivable = receivable
        create_chart.form.account_payable = payable
        if 'account_product' in modules:
            domain = [('company', '=', company.id)]
            if trytond_version >= (5, 1):
                domain.append(('type.revenue', '=', True))
            else:
                domain.append(('kind', '=', 'revenue'))
            if 'account_es' in modules:
                domain.append(('code', 'like', '7%'))
            revenue, = Account.find(domain, limit=1)
            domain = [('company', '=', company.id)]
            if trytond_version >= (5, 1):
                domain.append(('type.expense', '=', True))
            else:
                domain.append(('kind', '=', 'expense'))
            if 'account_es' in modules:
                domain.append(('code', 'like', '6%'))
            expense, = Account.find(domain, limit=1)
            create_chart.form.category_account_revenue = revenue
            create_chart.form.category_account_expense = expense
        create_chart.execute('create_properties')
    create_fiscalyear(company)
    today = datetime.date.today()
    if today.month <= 6:
        create_fiscalyear(company, today.year - 1)
    # TODO: Creata a initial move with some balance ??
    return company


def load_banks():
    Party = Model.get('party.party')
    Bank = Model.get('bank')
    if Bank.find():
        return

    logger.info('Creating Banks')

    parties = []
    for name in [
            'BANCO SANTANDER',
            'BANCA MARCH',
            'BANCO DE SABADELL',
            'BANKIA',
            'BANKINTER',
            'CITIBANK INTERNATIONAL PLC',
            'IBERCAJA BANCO',
            'CAIXABANK',
            ]:
        parties.append(Party(name=name, active=False))
    Party.save(parties)
    banks = []
    for party, bic in zip(parties, [
                'BSCHESMMXXX',
                'BMARES2MXXX',
                'BSABESBBXXX',
                'CAHMESMMXXX',
                'BKBKESMMXXX',
                'CITIESMXSEC',
                'CAZRES2ZXXX',
                'CAIXESBBXXX',
                ]):
        banks.append(Bank(bic=bic, party=party))
    Bank.save(banks)


def create_carriers(config):
    Party = Model.get('party.party')
    Carrier = Model.get('carrier')
    Template = Model.get('product.template')
    Category = Model.get('product.category')
    Uom = Model.get('product.uom')
    if Carrier.find([]):
        return

    logger.info('Creating Carriers')

    parties = []
    for name in [
            'SEUR GEOPOST SLU',
            'DHL PARCEL IBERICA SLU',
            'XPO',
            'A.T. TROTA',
            'PRIMAFRIO',
            'CORREOS',
            ]:
        parties.append(Party(name=name))
    Party.save(parties)
    account_category, = Category.find([('accounting', '=', True)], limit=1)
    unit, = Uom.find([('symbol', '=', 'u')])
    carrier_template = Template()
    carrier_template.name = 'Servicios Transporte'
    carrier_template.default_uom = unit
    carrier_template.type = 'service'
    carrier_template.purchasable = True
    carrier_template.salable = True
    carrier_template.list_price = Decimal('9.95')
    carrier_template.account_category = account_category
    carrier_template.save()
    carrier_product, = carrier_template.products
    carriers = []
    for party in parties:
        carriers.append(Carrier(party=party, carrier_product=carrier_product))
    Carrier.save(carriers)


def create_agents(config, company):
    Party = Model.get('party.party')
    Agent = Model.get('commission.agent')
    Template = Model.get('product.template')
    Category = Model.get('product.category')
    Uom = Model.get('product.uom')
    if Agent.find([]):
        return

    logger.info('Creating Agents')

    account_category, = Category.find([('accounting', '=', True)], limit=1)
    unit, = Uom.find([('symbol', '=', 'u')])
    template = Template()
    template.name = 'Commision'
    template.default_uom = unit
    template.type = 'service'
    template.list_price = Decimal(0)
    template.account_category = account_category
    template.save()
    commission_product, = template.products

    Plan = Model.get('commission.plan')
    plan = Plan(name='10%')
    plan.commission_product = commission_product
    plan.commission_method = 'payment'
    line = plan.lines.new()
    line.formula = 'amount * 0.1'
    plan.save()

    Country = Model.get('country.country')
    spain, = Country.find([('code', '=', 'ES')], limit=1)
    parties = []
    with open(os.path.join(base_path, 'names.txt'), 'r') as f:
        names = f.read().split('\n')
    names = [x.strip() for x in names if x.strip()]
    with open(os.path.join(base_path, 'surnames.txt'), 'r') as f:
        surnames = f.read().split('\n')
    with open(os.path.join(base_path, 'streets.txt'), 'r') as f:
        streets = f.read().split('\n')
    zip2phone = {
        '08': '93',
        '17': '972',
        '25': '973',
        '43': '977',
        }
    zips = list(zip2phone.keys())
    try:
        Bank = Model.get('bank')
        banks = Bank.find([])
    except KeyError:
        Bank = None
    parties = []
    for x in range(5):
        name = random.choice(names)
        surname1 = random.choice(surnames)
        surname2 = random.choice(surnames)
        street = random.choice(streets)
        name = '%s %s %s' % (name, surname1, surname2)
        street = '%s, %d' % (street, random.randrange(1, 100))
        zip_prefix = random.choice(zips)
        phone = get_phone(zip2phone.get(zip_prefix))
        mobile = get_phone('6')

        party = Party(name=name)
        address, = party.addresses
        address.street = street
        address.country = spain
        address.invoice = True
        address.delivery = True
        address.postal_code = get_valid_zip(zip_prefix)
        nif_prefix = random.choice(['4', '7'])
        nif_code = create_unique_nif(nif_prefix)
        party.identifiers.new(type='eu_vat', code=nif_code)
        if random.random() < 0.95:
            party.contact_mechanisms.new(type='phone', value=mobile)
        if random.random() < 0.8:
            party.contact_mechanisms.new(type='phone', value=phone)
        if Bank:
            account = party.bank_accounts.new()
            account.bank = random.choice(banks)
            number = account.numbers.new(type='iban')
            number.number = create_iban()
        parties.append(party)
    Party.save(parties)

    agents = []
    for party in parties:
        agent = Agent()
        agent.company = company
        agent.party = party
        agent.type_ = 'agent'
        agent.plan = plan
        agent.currency = company.currency
        agents.append(agent)
    Agent.save(agents)


def create_fiscalyear(company, year=None):
    """
    It creates a new fiscal year with monthly periods and the appropriate
    invoice sequences for the given company.

    If no year is specified the current year is used.
    """
    FiscalYear = Model.get('account.fiscalyear')
    Sequence = Model.get('ir.sequence')
    SequenceType = Model.get('ir.sequence.type')
    SequenceStrict = Model.get('ir.sequence.strict')

    if year is None:
        year = datetime.date.today().year
    date = datetime.date(int(year), 1, 1)
    if FiscalYear.find([
            ('company', '=', company.id),
            ('name', '=', '%s' % year),
            ]):
        return
    logger.info('Creating fiscalyear: %s' % year)
    type_account_move, = SequenceType.find([
            ('name', '=', 'Account Move')], limit=1)
    post_move_sequence = Sequence.find([
            ('name', '=', '%s' % year),
            ('sequence_type', '=', type_account_move.id),
            ('company', '=', company.id),
            ], limit=1)
    if post_move_sequence:
        post_move_sequence, = post_move_sequence
    else:
        post_move_sequence = Sequence(name='%s' % year,
            sequence_type=type_account_move, company=company, padding=5)
        post_move_sequence.save()

    fiscalyear = FiscalYear(name='%s' % year)
    fiscalyear.company = company
    fiscalyear.start_date = date + relativedelta(month=1, day=1)
    fiscalyear.end_date = date + relativedelta(month=12, day=31)
    fiscalyear.post_move_sequence = post_move_sequence
    fiscalyear_sequence, = fiscalyear.invoice_sequences
    type_account_invoice, = SequenceType.find([
            ('name', '=', 'Invoice')], limit=1)
    for attr, name in (('out_invoice_sequence', 'Customer Invoice'),
            ('in_invoice_sequence', 'Supplier Invoice'),
            ('out_credit_note_sequence', 'Customer Credit Note'),
            ('in_credit_note_sequence', 'Supplier Credit Note')):
        sequence = SequenceStrict.find([
                ('name', '=', '%s %s' % (name, year)),
                ('sequence_type', '=', type_account_invoice.id),
                ('company', '=', company.id),
                ])
        if sequence:
            sequence = sequence[0]
        else:
            prefix = 'FP-' if attr[:2] == 'in' else 'F-'
            sequence = SequenceStrict(company=company, padding=5,
                name='%s %s' % (name, year),
                prefix=prefix,
                sequence_type=type_account_invoice)
            sequence.save()
        setattr(fiscalyear_sequence, attr, sequence)
    fiscalyear.click('create_period')


def create_recurring_invoices(config, company):
    Invoice = Model.get('account.invoice')
    Tax = Model.get('account.tax')
    Party = Model.get('party.party')
    Account = Model.get('account.account')
    if Invoice.find(['type', '=', 'in']):
        return

    logger.info('Creating recurring invoices')
    suppliers = Party.find([], limit=50)
    today = datetime.date.today()
    tax, = Tax.find([
            ('group.kind', '=', 'purchase'),
            ('name', '=', 'IVA 21% (servicios)'),
            ('company', '=', company.id),
            ], limit=1)

    invoices = []

    supplier = random.choice(suppliers)
    account_627, = Account.find([('code', '=', '627')], limit=1)
    for month in range(1, today.month):
        first_day_of_month = datetime.datetime(today.year, month, 1)
        invoice = Invoice(type='in')
        invoice.party = supplier
        invoice.invoice_date = first_day_of_month
        line = invoice.lines.new()
        line.description = 'Movistar Telef. Comercial'
        line.quantity = 1
        line.account = account_627
        line.unit_price = Decimal(random.randrange(15, 25))
        line.taxes.append(Tax(tax.id))
        invoices.append(invoice)

    supplier = random.choice(suppliers)
    account_628, = Account.find([('code', '=', '628')], limit=1)
    for month in range(1, today.month):
        first_day_of_month = datetime.datetime(today.year, month, 25)
        invoice = Invoice(type='in')
        invoice.party = supplier
        invoice.invoice_date = first_day_of_month
        line = invoice.lines.new()
        line.description = 'Consumo Luz'
        line.quantity = 1
        line.account = account_628
        line.unit_price = Decimal(random.randrange(35, 80))
        line.taxes.append(Tax(tax.id))
        invoices.append(invoice)

    Invoice.save(invoices)
    invoices = random.sample(invoices, int(0.6 * len(invoices)))
    if invoices:
        Invoice.click(invoices, 'validate_invoice')
    invoices = random.sample(invoices, int(0.3 * len(invoices)))
    if invoices:
        Invoice.click(invoices, 'post')


def create_payment_terms(config):
    """
    It creates 3 payment terms:
    - 30 days
    - 60 days
    - 90 days
    """
    Term = Model.get('account.invoice.payment_term')
    if Term.find([]):
        return

    logger.info('Creating payment terms')

    terms = []
    term = Term(name='Contado')
    term.lines.new(type='remainder')
    terms.append(term)

    term = Term()
    term.name = '30 D'
    line = term.lines.new()
    line.relativedeltas.new(days=30)
    terms.append(term)

    term = Term()
    term.name = '60 D'
    line = term.lines.new()
    line.relativedeltas.new(days=60)
    terms.append(term)

    term = Term()
    term.name = '90 D'
    line = term.lines.new()
    line.relativedeltas.new(days=90)
    terms.append(term)

    Term.save(terms)


def create_payment_types(config, company, language='es'):
    Type = Model.get('account.payment.type')
    if Type.find([]):
        return
    logger.info('Creating payment types')
    names = {
        'bank-transfer': {
            'ca': 'Transferència Bancària',
            'en': 'Bank Transfer',
            'es': 'Transferencia Bancaria',
            },
        'direct-debit': {
            'ca': 'Domiciliació bancària',
            'en': 'Direct Debit',
            'es': 'Domiciliación bancaria',
            },
        'cash': {
            'ca': 'Efectiu',
            'en': 'Cash',
            'es': 'Efectivo',
            },
        'credit-card': {
            'ca': 'Targeta de crèdit',
            'en': 'Credit Card',
            'es': 'Tarjeta de crédito',
            },
        }

    t = Type()
    t.name = names['bank-transfer'][language]
    t.kind = 'receivable'
    if hasattr(t, 'account_bank'):
        t.account_bank = 'company'
    t.save()

    t = Type()
    t.name = names['direct-debit'][language]
    t.kind = 'receivable'
    if hasattr(t, 'account_bank'):
        t.account_bank = 'party'
    t.save()

    t = Type()
    t.name = names['cash'][language]
    t.kind = 'both'
    if hasattr(t, 'account_bank'):
        t.account_bank = 'none'
    t.save()

    t = Type()
    t.name = names['bank-transfer'][language]
    t.kind = 'payable'
    if hasattr(t, 'account_bank'):
        t.account_bank = 'party'
    t.save()

    t = Type()
    t.name = names['direct-debit'][language]
    t.kind = 'payable'
    if hasattr(t, 'account_bank'):
        t.account_bank = 'company'
    t.save()


def create_sepa_journals(config, company):

    PaymentJournal = Model.get('account.payment.journal')
    if PaymentJournal.find([]):
        return
    try:
        PaymentType = Model.get('account.payment.type')
        payment_type, = PaymentType.find([
                ('kind', '=', 'receivable'),
                ('account_bank', '=', 'party'),
                ], limit=1)
    except KeyError:
        payment_type = None
    logger.info('Creating sepa payment journals')
    journals = []
    for bank_account in company.party.bank_accounts:
        if random.random() < 0.3:
            continue
        payment_journal = PaymentJournal()
        payment_journal.company = company
        payment_journal.name = 'SEPA %s' % (len(journals) + 1)
        payment_journal.process_method = 'sepa'
        if payment_type:
            payment_journal.payment_type = payment_type
        payment_journal.sepa_bank_account_number = bank_account.numbers[0]
        payment_journal.sepa_payable_flavor = 'pain.001.001.03'
        payment_journal.sepa_receivable_flavor = 'pain.008.001.02'
        journals.append(payment_journal)
    PaymentJournal.save(journals)


def create_dunning_procedures(config):
    Procedure = Model.get('account.dunning.procedure')
    Configuration = Model.get('account.configuration')
    if Procedure.find([]):
        return

    logger.info('Creating dunning procedures')

    procedure = Procedure()
    procedure.name = 'Por defecto'
    procedure.levels.new(overdue=datetime.timedelta(10))
    procedure.levels.new(overdue=datetime.timedelta(30))
    procedure.levels.new(overdue=datetime.timedelta(60))
    procedure.save()

    config = Configuration(1)
    config.default_dunning_procedure = procedure
    config.save()


def create_parties(config, modules):
    Party = Model.get('party.party')
    Country = Model.get('country.country')
    Lang = Model.get('ir.lang')

    try:
        PaymentTerm = Model.get('account.invoice.payment_term')
        terms = PaymentTerm.find([])
    except KeyError:
        PaymentTerm = None
    try:
        PaymentType = Model.get('account.payment.type')
        types = PaymentType.find([])
        payable_types = [t for t in types
            if t.kind in ['both', 'payable']]
        receivable_types = [t for t in types
            if t.kind in ['both', 'receivable']]
    except KeyError:
        PaymentType = None
    try:
        Bank = Model.get('bank')
        banks = Bank.find([])
    except KeyError:
        Bank = None
    try:
        Mandate = Model.get('account.payment.sepa.mandate')
    except KeyError:
        Mandate = None

    price_lists = []
    if 'sale_price_list' in modules:
        PriceList = Model.get('product.price_list')
        price_lists = PriceList.find([])

    parties = Party.find([], limit=50)
    if len(parties) == 50:
        return
    logger.info('Creating parties')

    spain, = Country.find([('code', '=', 'ES')], limit=1)

    with open(os.path.join(base_path, 'companies.txt'), 'r') as f:
        companies = f.read().split('\n')
    companies = [x.strip() for x in companies if x.strip()]
    companies = random.sample(companies, 200)
    with open(os.path.join(base_path, 'streets.txt'), 'r') as f:
        streets = f.read().split('\n')
    languages = Lang.find([
            ('code', 'in', ['ca', 'es', 'en']),
            ])
    zip2phone = {
        '08': '93',
        '17': '972',
        '25': '973',
        '43': '977',
        }
    today = datetime.date.today()
    zips = list(zip2phone.keys())
    parties, mandates = [], []
    for company in companies:
        street = random.choice(streets)
        street = '%s, %d' % (street, random.randrange(1, 100))

        zip_prefix = random.choice(zips)
        phone = get_phone(zip2phone.get(zip_prefix))
        mobile = get_phone('6')

        party = Party(name=company)
        address, = party.addresses
        address.street = street
        address.country = spain
        address.invoice = True
        address.delivery = True
        address.postal_code = get_valid_zip(zip_prefix)
        cif_prefix = 'B'
        if 'S.A.' in company or 'S. A.' in company:
            cif_prefix = 'A'
        cif_prefix += zip_prefix
        cif_code = create_unique_cif(cif_prefix)
        party.identifiers.new(type='eu_vat', code=cif_code)
        if random.random() < 0.8:
            party.contact_mechanisms.new(type='phone', value=phone)
        if random.random() < 0.5:
            party.contact_mechanisms.new(type='phone', value=mobile)
        party.lang = random.choice(languages)
        if PaymentTerm:
            party.supplier_payment_term = None
            party.customer_payment_term = None
        if PaymentType:
            party.supplier_payment_type = None
            party.customer_payment_type = None
        create_bank_account = random.random() < 0.5 if Bank else False
        if PaymentTerm and random.random() < 0.3:
            party.supplier_payment_term = random.choice(terms)
            if PaymentType:
                party.supplier_payment_type = random.choice(payable_types)
                if party.supplier_payment_type.account_bank == 'party':
                    create_bank_account = True
        if PaymentTerm and random.random() < 0.7:
            party.customer_payment_term = random.choice(terms)
            if PaymentType:
                party.customer_payment_type = random.choice(receivable_types)
                if party.customer_payment_type.account_bank == 'party':
                    create_bank_account = True
        if Bank and create_bank_account:
            account = party.bank_accounts.new()
            account.bank = random.choice(banks)
            number = account.numbers.new(type='iban')
            number.number = create_iban()
        if price_lists and random.random() < 0.5:
            party.sale_price_list = random.choice(price_lists)
        parties.append(party)
    Party.save(parties)
    if not Mandate:
        return parties
    mandates = []
    for party in parties:
        if party.bank_accounts:
            mandate = Mandate()
            mandate.party = party
            mandate.account_number = party.bank_accounts[0].numbers[0]
            mandate.identification = 'MAN%s' % (party.code)
            mandate.signature_date = random_datetime(
                today - relativedelta(years=1),
                today - relativedelta(months=3))
            mandates.append(mandate)
    Mandate.save(mandates)
    Mandate.click(mandates, 'request')
    Mandate.click(mandates, 'validate_mandate')
    return parties


def create_employees(config, company):
    Party = Model.get('party.party')
    Country = Model.get('country.country')
    Employee = Model.get('company.employee')

    if Employee.find([]):
        return
    logger.info('Creating employees')

    spain, = Country.find([('code', '=', 'ES')], limit=1)

    with open(os.path.join(base_path, 'names.txt'), 'r') as f:
        names = f.read().split('\n')
    names = [x.strip() for x in names if x.strip()]
    with open(os.path.join(base_path, 'surnames.txt'), 'r') as f:
        surnames = f.read().split('\n')
    with open(os.path.join(base_path, 'streets.txt'), 'r') as f:
        streets = f.read().split('\n')
    zip2phone = {
        '08': '93',
        '17': '972',
        '25': '973',
        '43': '977',
        }
    zips = list(zip2phone.keys())
    try:
        Bank = Model.get('bank')
        banks = Bank.find([])
    except KeyError:
        Bank = None
    parties = []
    for x in range(10):
        name = random.choice(names)
        surname1 = random.choice(surnames)
        surname2 = random.choice(surnames)
        street = random.choice(streets)
        name = '%s %s %s' % (name, surname1, surname2)
        street = '%s, %d' % (street, random.randrange(1, 100))
        zip_prefix = random.choice(zips)
        phone = get_phone(zip2phone.get(zip_prefix))
        mobile = get_phone('6')

        party = Party(name=name)
        address, = party.addresses
        address.street = street
        address.country = spain
        address.invoice = True
        address.delivery = True
        address.postal_code = get_valid_zip(zip_prefix)
        nif_prefix = random.choice(['4', '7'])
        nif_code = create_unique_nif(nif_prefix)
        party.identifiers.new(type='eu_vat', code=nif_code)
        if random.random() < 0.95:
            party.contact_mechanisms.new(type='phone', value=mobile)
        if random.random() < 0.8:
            party.contact_mechanisms.new(type='phone', value=phone)
        if Bank:
            account = party.bank_accounts.new()
            account.bank = random.choice(banks)
            number = account.numbers.new(type='iban')
            number.number = create_iban()
        parties.append(party)
    Party.save(parties)
    employees = []
    for party in parties:
        employee = Employee(company=company, party=party)
        employees.append(employee)
    Employee.save(employees)


def create_product_categories(config):
    Category = Model.get('product.category')
    if Category.find([]):
        return
    logger.info('Creating product categories')
    categories = []
    for parent_name in 'ABCDEFG':
        category = Category(name=parent_name)
        for x in range(random.randint(2, 8)):
            child_name = '%s-%s' % (parent_name, x)
            category.childs.new(name=child_name)
        categories.append(category)
    Category.save(categories)


def create_products(config, company, modules):
    """
    Creates the 'count' first products from the icecat database in catalog.xml.
    """
    Category = Model.get('product.category')
    Template = Model.get('product.template')
    Unit = Model.get('product.uom')
    unit = Unit(1)
    if Template.find([]):
        return
    logger.info('Creating products')
    # Pick categories from a root level
    if 'account_product' in modules:
        categories = Category.find([
                ('accounting', '=', False),
                ('parent', '!=', None),
                ])
        account_category = Category.find([('accounting', '=', True)])
        if account_category:
            account_category = account_category[0]
        else:
            account_category = Category(name='Todos los productos',
                accounting=True)
            Account = Model.get('account.account')
            Tax = Model.get('account.tax')
            if 'account_es' in modules:
                account_category.account_revenue, = Account.find([
                        ('code', '=', '700'),
                        ('company', '=', company.id),
                        ], limit=1)
                account_category.account_expense, = Account.find([
                        ('code', '=', '600'),
                        ('company', '=', company.id),
                        ], limit=1)
                tax, = Tax.find([
                        ('group.kind', '=', 'sale'),
                        ('name', '=', 'IVA 21% (bienes)'),
                        ('company', '=', company.id),
                        ], limit=1)
                account_category.customer_taxes.append(tax)
                tax, = Tax.find([
                        ('group.kind', '=', 'purchase'),
                        ('name', '=', 'IVA 21% (bienes)'),
                        ('company', '=', company.id),
                        ], limit=1)
                account_category.supplier_taxes.append(tax)
            account_category.save()
    else:
        account_category = None

    import xmltodict
    with open(os.path.join(base_path, 'catalog.xml'), 'r') as f:
        xml = xmltodict.parse(f.read())

    i = 0
    products = []
    for item in xml.get('ICECAT-interface').get('files.index').get('file'):
        template = Template()
        template.name = item.get('@Model_Name')
        cost_price = Decimal(random.randrange(1, 100))
        template.list_price = cost_price * (
            1 + Decimal(random.randrange(1, 100)) / 100)
        template.default_uom = unit
        if account_category:
            template.account_category = account_category
        template.categories.append(Category(random.choice(categories).id))
        if 'sale' in modules:
            template.salable = True
        if 'purchase' in modules:
            template.purchasable = True

        product, = template.products
        product.code = item.get('@Product_ID')
        product.cost_price = cost_price
        products.append(template)
        i += 1
        if i >= 50:
            break
    Template.save(products)


def create_product_supplier(config):
    ProductSupplier = Model.get('purchase.product_supplier')
    Product = Model.get('product.product')
    Party = Model.get('party.party')

    if ProductSupplier.find([]):
        return

    logger.info('Creating product suppliers')

    lead_times = [None, datetime.timedelta(1), datetime.timedelta(2)]

    products = Product.find([
            ('type', '=', 'goods'),
            ('consumable', '=', False),
            ('purchasable', '=', True),
            ])
    suppliers = Party.find([], limit=50)
    supplier = None

    to_save = []
    for product in products:
        if random.random() > 0.8:
            continue
        # Allow more than one product for the same supplier
        if random.random() < 0.3:
            supplier = None

        if supplier is None:
            supplier = random.choice(suppliers)
        product_supplier = ProductSupplier()
        product_supplier.product = product
        product_supplier.party = supplier
        product_supplier.lead_time = random.choice(lead_times)
        to_save.append(product_supplier)
    ProductSupplier.save(to_save)


def setup_production_routing(config, activated, company):
    Routing = Model.get('production.routing')
    Operation = Model.get('production.routing.operation')

    routings = Routing.find([])
    if routings:
        return routings

    logger.info('Creating production routings')

    routing = Routing(name='Ordenador')
    routing2 = Routing(name='Ordenador sin software')

    operation1 = Operation(name='Ensamblar piezas')
    operation1.save()
    step1 = routing.steps.new()
    step1.operation = operation1
    step1_2 = routing2.steps.new()
    step1_2.operation = operation1

    operation2 = Operation(name='Instalar software')
    operation2.save()
    step2 = routing.steps.new()
    step2.operation = operation2

    operation3 = Operation(name='Test')
    operation3.save()
    step3 = routing.steps.new()
    step3.operation = operation3

    operation4 = Operation(name='Empaquetar')
    operation4.save()
    step4 = routing.steps.new()
    step4.operation = operation4
    step4_2 = routing2.steps.new()
    step4_2.operation = operation4
    routing.save()
    routing2.save()

    return [routing, routing2]


def setup_production_work(config, activated, company):
    WorkCenterCategory = Model.get('production.work.center.category')
    WorkCenter = Model.get('production.work.center')
    Operation = Model.get('production.routing.operation')

    if WorkCenter.find([]):
        return

    logger.info('Creating production work centers')

    assembly = WorkCenterCategory(name='Ensamblaje')
    assembly.save()
    operation, = Operation.find([('name', '=', 'Ensamblar piezas')])
    operation.work_center_category = assembly
    operation.save()

    installation = WorkCenterCategory(name='Instalación')
    installation.save()
    operations = Operation.find([
            ('name', 'in', ['Instalar software', 'Test']),
            ])
    for operation in operations:
        operation.work_center_category = installation
    Operation.save(operations)

    packaging = WorkCenterCategory(name='Empaquetado')
    packaging.save()
    operation, = Operation.find([('name', '=', 'Empaquetar')])
    operation.work_center_category = packaging
    operation.save()

    lines = []
    for i in range(1, 4):
        line = WorkCenter(name='Linea %i' % i)

        assembly_line = line.children.new()
        assembly_line.name = 'Ensamblado Linea %i' % i
        assembly_line.category = assembly
        assembly_line.cost_method = 'cycle'
        assembly_line.cost_price = Decimal(20)

        installation_line = line.children.new()
        installation_line.name = 'Instalación Linea %i' % i
        installation_line.category = installation
        installation_line.cost_method = 'hour'
        installation_line.cost_price = Decimal(15)

        packaging_line = line.children.new()
        packaging_line.name = 'Empaquetado Linea %i' % i
        packaging_line.category = packaging
        packaging_line.cost_method = 'hour'
        packaging_line.cost_price = Decimal(10)

        lines.append(line)
    WorkCenter.save(lines)


def setup_production(config, modules, routings):
    BOM = Model.get('production.bom')
    ProductTemplate = Model.get('product.template')
    Product = Model.get('product.product')
    Bom = Model.get('production.bom')
    Category = Model.get('product.category')
    Uom = Model.get('product.uom')

    if BOM.find([], limit=1):
        return

    logger.info("Creating production BOMs")

    unit, = Uom.find([('name', '=', 'Unit')])

    if 'account_product' in modules:
        account_category, = Category.find([('accounting', '=', True)], limit=1)

    def create_product(name, list_price, cost_price, output=False):
        template = ProductTemplate(name=name)
        template.default_uom = unit
        template.type = 'goods'
        template.list_price = list_price
        if 'account_product' in modules:
            template.account_category = account_category
        if 'purchase' in modules and not output:
            template.purchasable = True
        if output:
            template.producible = True
            if 'sale' in modules:
                template.salable = True
        product, = template.products
        product.cost_price = cost_price
        template.save()
        return template.products[0]

    tower = create_product('Carcasa Ordenador', Decimal(200), Decimal(120))
    keyboard = create_product('Teclado', Decimal(30), Decimal(10))
    mouse = create_product('Raton', Decimal(10), Decimal(5))
    screen = create_product('Pantalla', Decimal(180), Decimal(70))
    ram = create_product('Memoria RAM 8 GB', Decimal(40), Decimal(10))
    ssd = create_product('Disco Duro SSD', Decimal(40), Decimal(10))

    base_list_price = Decimal(600)
    base_cost_price = Decimal(365)

    boms = []
    for ram_size in [8, 16]:
        for disk_size in [128, 256, 512]:

            product_name = 'Ordenador %dGB RAM, SSD %dGB' % (
                ram_size, disk_size)
            bom = BOM(name=product_name)

            input_ = bom.inputs.new()
            input_.product = tower
            input_.quantity = 1

            input_ = bom.inputs.new()
            input_.product = keyboard
            input_.quantity = 1

            input_ = bom.inputs.new()
            input_.product = mouse
            input_.quantity = 1

            input_ = bom.inputs.new()
            input_.product = screen
            input_.quantity = 1

            input_ = bom.inputs.new()
            input_.product = ram
            input_.quantity = (ram_size / 8)

            input_ = bom.inputs.new()
            input_.product = ssd
            input_.quantity = (disk_size / 128)

            output = bom.outputs.new()
            computer = create_product(
                product_name,
                (base_list_price + Decimal((ram_size / 8) * 20)
                    + Decimal((disk_size / 128) * 40)),
                (base_cost_price + Decimal((ram_size / 8) * 5)
                    + Decimal((disk_size / 128) * 15)),
                True,
                )
            output.product = computer
            output.quantity = 1
            boms.append(bom)

    Bom.save(boms)
    products = []
    for bom in boms:
        product = bom.outputs[0].product
        product_bom = product.boms.new()
        product_bom.bom = bom
        if 'production_routing' in modules:
            product_bom.routing = random.choice(routings)
            if bom not in product_bom.routing.boms:
                routing = product_bom.routing
                routing.boms.append(bom)
                routing.save()
        products.append(product)
    Product.save(products)


def create_price_lists(config):
    PriceList = Model.get('product.price_list')
    Category = Model.get('product.category')
    if PriceList.find([]):
        return
    logger.info('Creating product price lists')
    price_lists = []
    price_list = PriceList()
    price_list.price = 'list_price'
    price_list.name = 'Cliente Habitual'
    for category in Category.find([('parent', '=', None)]):
        if random.random() > 0.6:
            line = price_list.lines.new()
            line.category = category
            line.formula = 'unit_price * 0.85'
    line = price_list.lines.new()
    line.formula = 'unit_price * 0.9'
    price_lists.append(price_list)
    price_list = PriceList()
    price_list.price = 'list_price'
    price_list.name = 'Cliente VIP'
    for category in Category.find([('parent', '=', None)]):
        if random.random() > 0.6:
            line = price_list.lines.new()
            line.category = category
            line.formula = 'unit_price * 0.75'
    line = price_list.lines.new()
    line.formula = 'unit_price * 0.8'
    price_lists.append(price_list)
    PriceList.save(price_lists)


def create_assets(config, company, modules):
    Asset = Model.get('account.asset')
    Account = Model.get('account.account')
    Category = Model.get('product.category')
    Template = Model.get('product.template')
    Unit = Model.get('product.uom')
    unit = Unit(1)
    today = datetime.date.today()
    if Asset.find([]):
        return
    logger.info('Creating asset products')
    account_category, = Category.find([('accounting', '=', True)], limit=1)
    asset_category = Category.find([
            ('name', '=', 'Maquinaria'),
            ('accounting', '=', True),
            ])
    if asset_category:
        asset_category, = asset_category
    else:
        asset_category = Category(
            name='Maquinaria', accounting=True, parent=account_category,
            taxes_parent=True)
        if 'account_es' in modules:
            asset_category.account_expense, = Account.find([
                    ('code', '=', '681'),
                    ('company', '=', company.id),
                    ], limit=1)
            asset_category.account_depreciation, = Account.find([
                    ('code', '=', '2813'),
                    ('company', '=', company.id),
                    ], limit=1)
            asset_category.account_asset, = Account.find([
                    ('code', '=', '213'),
                    ('company', '=', company.id),
                    ], limit=1)
            # Fix for account_es: TODO Remove in 5.4
            if not asset_category.account_depreciation.type.fixed_asset:
                type_ = asset_category.account_depreciation.type
                type_.assets = True
                type_.fixed_asset = True
                type_.save()
            if not asset_category.account_asset.type.fixed_asset:
                type_ = asset_category.account_asset.type
                type_.assets = True
                type_.fixed_asset = True
                type_.save()
        asset_category.save()

    products = []
    for i in range(10):
        template = Template()
        template.name = 'Maquina %d' % (i + 1)
        cost_price = Decimal(random.randrange(1, 100))
        template.list_price = cost_price * (
            1 + Decimal(random.randrange(1, 100)) / 100)
        template.default_uom = unit
        template.account_category = asset_category
        template.type = 'assets'
        template.depreciable = True
        template.depreciation_duration = random.randint(3, 10) * 12
        product, = template.products
        product.cost_price = cost_price
        products.append(template)
    Template.save(products)
    products = [x for x in chain(*(t.products for t in products))]
    assets = []
    for product in products:
        asset = Asset()
        asset.product = product
        asset.company = company
        asset.product = product
        asset.quantity = 1
        asset.unit = product.default_uom
        asset.value = Decimal(random.randrange(1000, 10000))
        asset.residual_value = Decimal(0)
        asset.purchase_date = random_datetime(
            today - relativedelta(months=6),
            today)
        asset.start_date = asset.purchase_date
        if random.random() > 0.5:
            asset.frequency = 'yearly'
        assets.append(asset)
    Asset.click(assets, 'run')


def create_lots(config, modules):
    Lot = Model.get('stock.lot')
    Template = Model.get('product.template')

    if Lot.find([]):
        return

    logger.info("Creating lots")

    goods = Template.find([('type', '=', 'goods')])
    lots = random.sample(goods, int(0.6 * len(goods)))
    products = [x for x in chain(*(t.products for t in lots))]
    lots = []
    for product in products:
        for _ in range(random.randrange(1, 9)):
            lot = Lot()
            lot.product = product
            lot.number = ('%s' % len(lots)).rjust(5, '0')
            lots.append(lot)
    Lot.save(lots)


def create_inventory(config):
    Inventory = Model.get('stock.inventory')
    Location = Model.get('stock.location')
    Product = Model.get('product.product')
    location = Location.find([('type', '=', 'warehouse')])[0].storage_location

    if Inventory.find([]):
        return
    try:
        Lot = Model.get('stock.lot')
        lots = Lot.find([])
    except KeyError:
        Lot = None

    logger.info('Creating inventory for products')

    products = Product.find([
            ('type', '=', 'goods'),
            ('consumable', '=', False),
            ])
    products = random.sample(products, int(0.7 * len(products)))

    inventory = Inventory()
    today = datetime.date.today()
    delta = relativedelta(day=1, month=1)
    if today.month <= 6:
        delta = relativedelta(day=1, month=7, years=-1)
    inventory.date = today + delta
    inventory.location = location
    for product in products:
        # If lot is required create a separate line with lot information
        if Lot and product.lot_required:
            continue
        inventory_line = inventory.lines.new()
        inventory_line.product = product
        inventory_line.quantity = float(random.randrange(50))
        inventory_line.expected_quantity = 0.0
    if Lot:
        for lot in lots:
            inventory_line = inventory.lines.new()
            inventory_line.product = lot.product
            inventory_line.lot = lot
            inventory_line.quantity = float(random.randrange(10))
            inventory_line.expected_quantity = 0.0
    inventory.click('confirm')


def create_order_points(config, modules):
    OrderPoint = Model.get('stock.order_point')
    Product = Model.get('product.product')
    Location = Model.get('stock.location')

    if OrderPoint.find([]):
        return

    logger.info("Creating order points")

    products = Product.find([
            ('type', '=', 'goods'),
            ('consumable', '=', False),
            ('purchasable', '=', True),
            ])
    warehouse, = Location.find([('type', '=', 'warehouse')])

    to_save = []
    for product in products:
        if random.random() > 0.6:
            continue
        order_point = OrderPoint()
        order_point.type = 'purchase'
        order_point.product = product
        order_point.warehouse_location = warehouse
        order_point.min_quantity = int(random.random() * 20)
        order_point.target_quantity = (
            int(random.random() * 50) + order_point.min_quantity)
        to_save.append(order_point)

    if 'stock_supply_production' in modules:
        products = Product.find([
                ('type', '=', 'goods'),
                ('consumable', '=', False),
                ('producible', '=', True),
                ])

        for product in products:
            if random.random() > 0.6:
                continue
            order_point = OrderPoint()
            order_point.type = 'production'
            order_point.product = product
            order_point.warehouse_location = warehouse
            order_point.min_quantity = int(random.random() * 20)
            order_point.target_quantity = (
                int(random.random() * 50) + order_point.min_quantity)
            to_save.append(order_point)

    OrderPoint.save(to_save)


def supply_stock(config, modules):
    if 'stock_supply_production' in modules:
        Warning_ = Model.get('res.user.warning')
        User = Model.get('res.user')
        warning_ = Warning_()
        warning_.name = 'stock.supply@%s' % datetime.date.today()
        warning_.user = User(get_config().user)
        warning_.save()
    create_pr = Wizard('stock.supply')
    create_pr.execute('create_')


def create_purchases(config, modules):
    Purchase = Model.get('purchase.purchase')
    Party = Model.get('party.party')
    Product = Model.get('product.product')
    today = datetime.date.today()

    if Purchase.find([]):
        return

    logger.info('Creating purchases')

    parties = Party.find([])
    products = Product.find([('purchasable', '=', True)])
    purchases = []
    for c in range(50):
        purchase = Purchase()
        purchase.party = random.choice(parties)
        for lc in range(random.randrange(1, 9)):
            line = purchase.lines.new()
            line.product = random.choice(products)
            line.quantity = random.randrange(1, 20)
            line.unit_price = line.product.cost_price
        purchases.append(purchase)
    Purchase.save(purchases)
    to_quote = []
    for purchase in purchases:
        if random.random() > 0.9:
            continue
        purchase.purchase_date = random_datetime(
            today, today + relativedelta(months=6))
        to_quote.append(purchase)
    if to_quote:
        Purchase.click(to_quote, 'quote')
    cancel = random.sample(to_quote, int(0.2 * len(to_quote)))
    if cancel:
        Purchase.click(cancel, 'cancel')
    purchases = list(set(to_quote) - set(cancel))
    purchases = random.sample(purchases, int(0.8 * len(purchases)))
    if purchases:
        Purchase.click(purchases, 'confirm')
    purchases = random.sample(purchases, int(0.8 * len(purchases)))
    if purchases:
        Purchase.click(purchases, 'process')


def process_purchase_shipments(config, modules):
    Purchase = Model.get('purchase.purchase')
    ShipmentIn = Model.get('stock.shipment.in')

    if ShipmentIn.find([]):
        return
    logger.info('Creating supplier shipments')

    try:
        Carrier = Model.get('carrier')
        carriers = Carrier.find([])
    except KeyError:
        carriers = []
    try:
        Lot = Model.get('stock.lot')
        lots = defaultdict(list)
        for lot in Lot.find([]):
            lots[lot.product].append(lot)
    except KeyError:
        lots = {}
    suppliers = defaultdict(list)
    for purchase in Purchase.find([
                ('state', '=', 'processing'),
                ]):
        suppliers[purchase.party].append(purchase)
    today = datetime.date.today()

    to_process = []
    for supplier, purchases in suppliers.items():
        for purchase in purchases:
            shipment = ShipmentIn()
            shipment.supplier = supplier
            all_moves = shipment.incoming_moves.find()
            # Normally supplier sends all of the moves
            if random.choice([True, True, False]):
                moves = all_moves
            else:
                moves = random.sample(all_moves, int(0.6 * len(all_moves)))
            while moves:
                shipment = ShipmentIn()
                shipment.supplier = supplier
                date = purchase.purchase_date
                shipment.effective_date = min(
                    random_datetime(date - relativedelta(days=15), date),
                    today)
                if 'purchase_shipment_cost' in modules and carriers:
                    shipment.carrier = random.choice(carriers)
                for _ in range(random.randint(1, max(len(moves), 2))):
                    if not moves:
                        break
                    move = moves.pop()
                    if lots:
                        if move.product in lots:
                            move.lot = random.choice(lots[move.product])
                        else:
                            continue
                    shipment.incoming_moves.append(move)
                to_process.append(shipment)
    ShipmentIn.click(to_process, 'receive')
    ShipmentIn.click(to_process, 'do')


def create_productions(config, modules):
    Production = Model.get('production')
    Product = Model.get('product.product')

    if Production.find([('state', '!=', 'request')], limit=1):
        return

    logger.info("Creating productions")

    today = datetime.date.today()
    products = Product.find([
            ('type', '=', 'goods'),
            ('consumable', '=', False),
            ('producible', '=', True),
            ])

    if 'production_work' in modules:
        WorkCenter = Model.get('production.work.center')
        WorkCycle = Model.get('production.work.cycle')
        work_centers = WorkCenter.find([('parent', '=', None)])

    production_date = today + relativedelta(days=-15)
    productions = []
    while production_date <= today + relativedelta(days=10):
        for _ in range(random.randint(0, 3)):
            production = Production()
            production.planned_date = production_date
            production.product = random.choice(products)
            production.quantity = random.randint(1, 5)
            production.bom = production.product.boms[0].bom

            if 'production_routing' in modules:
                production.routing = production.product.boms[0].routing

            if 'production_work' in modules:
                production.work_center = random.choice(work_centers)

            productions.append(production)
        production_date += relativedelta(days=random.randint(1, 3))

    Production.save(productions)

    to_wait = []
    for production in productions:
        if (production.planned_date < today) or (random.random() <= 2. / 3.):
            to_wait.append(production)
    Production.click(to_wait, 'wait')

    to_do = []
    for production in to_wait:
        if production.planned_date < today:
            if not production.click('assign_try'):
                continue
            production.click('run')
            if random.random() <= 0.8:
                if 'production_work' in modules:
                    for work in production.works:
                        for _ in range(0, random.randint(1, 2)):
                            cycle = WorkCycle(
                                work=work,
                                duration=datetime.timedelta(
                                    seconds=random.randint(60, 3600)),
                                )
                            cycle.save()
                            cycle.click('run')
                            cycle.click('do')
                output, = production.outputs
                output.unit_price = (production.cost
                    / Decimal(production.quantity)
                    ).quantize(Decimal('0.0001'))
                to_do.append(production)
    Production.click(to_do, 'done')


def create_opportunities(config, modules):
    """
    It randomly creates leads and opportunities

    It creates 'count' leads.
    - It converts 80% of the converted leads into opportunities
    - It converts 40% of the opportunities as lost
    - It sets 80% of the remaining opportunities are converted.
    """
    Opportunity = Model.get('sale.opportunity')
    Product = Model.get('product.product')
    Party = Model.get('party.party')
    Employee = Model.get('company.employee')
    today = datetime.date.today()

    if 'sale_opportunity_activity' in modules:
        Activity = Model.get('activity.activity')
        ActivityType = Model.get('activity.type')
        activity_types = ActivityType.find([])
        ActivityReference = Model.get('activity.reference')
        if not ActivityReference.find([
                    ('model.model', '=', 'sale.opportunity'),
                    ]):
            IRModel = Model.get('ir.model')
            opp_model, = IRModel.find([('model', '=', 'sale.opportunity')])
            reference = ActivityReference(model=opp_model)
            reference.save()
    else:
        Activity = None

    if Opportunity.find([]):
        return

    logger.info('Creating sale opportunities')

    employees = Employee.find([])
    parties = Party.find([('id', 'not in', [e.party.id for e in employees])])
    products = Product.find([
        ('salable', '=', True),
        ])
    products = [p for p in products if p.list_price]

    opportunities = []
    for x in range(150):
        opp = Opportunity()
        party = random.choice(parties)
        product = random.choice(products)
        opp.description = '%s - %s' % (party.rec_name, product.rec_name)
        opp.party = party
        if party.addresses:
            opp.address = party.addresses[0]
        opp.employee = random.choice(employees)
        opp.company = opp.employee.company
        opp.start_date = random_datetime(today - relativedelta(months=2),
            today)
        opp.conversion_probability = random.randrange(1, 9) / 10.0
        opp.amount = Decimal(random.randrange(1, 250) * 100)

        for lc in range(random.randrange(1, 8)):
            line = opp.lines.new()
            line.product = random.choice(products)
            line.quantity = random.randrange(1, 20)
        opportunities.append(opp)
    Opportunity.save(opportunities)
    if Activity:
        activities = []
        now = datetime.datetime.now()
        for opportunity in opportunities:
            for x in range(random.randrange(0, 5)):
                activity = Activity()
                activity.resource = opportunity
                activity.subject = 'Contacto'
                activity.party = opp.party
                activity.employee = opp.employee
                activity.activity_type = random.choice(activity_types)
                activity.dtstart = random_datetime(
                    datetime.datetime.combine(
                        opp.start_date, datetime.time.min),
                    now + relativedelta(months=1))
                if activity.dtstart < now:
                    activity.dtend = activity.dtstart + relativedelta(hours=2)
                    activity.state = 'held'
                else:
                    activity.state = 'planned'
                activities.append(activity)
        Activity.save(activities)
    opportunities = random.sample(opportunities, int(0.9 * len(opportunities)))
    if opportunities:
        Opportunity.click(opportunities, 'opportunity')

    lost = random.sample(opportunities, int(0.2 * len(opportunities)))
    if lost:
        for opportunity in lost:
            if not opportunity.start_date:
                continue
            opportunity.end_date = random_datetime(
                opportunity.start_date + relativedelta(days=3),
                opportunity.start_date + relativedelta(days=20),
                )
        Opportunity.click(lost, 'lost')

    non_lost = list(set(opportunities) - set(lost))
    opportunities = random.sample(non_lost, int(0.8 * len(non_lost)))
    if opportunities:
        for opportunity in opportunities:
            if not opportunity.start_date:
                continue
            opportunity.end_date = random_datetime(
                today + relativedelta(days=1),
                today + relativedelta(months=2),
                )
        Opportunity.click(opportunities, 'convert')


def create_sales(config, modules):
    Sale = Model.get('sale.sale')
    Party = Model.get('party.party')
    Product = Model.get('product.product')

    if Sale.find([]):
        return

    try:
        Agent = Model.get('commission.agent')
        agents = Agent.find([])
    except KeyError:
        Agent = None
    try:
        Carrier = Model.get('carrier')
        carriers = Carrier.find([])
    except KeyError:
        Carrier = None

    logger.info('Creating sales')

    parties = Party.find([])
    products = Product.find([('salable', '=', True)])
    sales = []
    for c in range(70):
        sale = Sale()
        sale.party = random.choice(parties)
        for lc in range(random.randrange(1, 9)):
            line = sale.lines.new()
            line.product = random.choice(products)
            line.quantity = random.randrange(1, 20)
            line.unit_price = Decimal(random.randrange(1, 10))
        if Agent:
            sale.agent = random.choice(agents)
        if 'sale_shipment_cost' in modules and not sale.carrier:
            sale.carrier = random.choice(carriers)
        sales.append(sale)
    Sale.save(sales)


def create_payments(config, company):
    Currency = Model.get('currency.currency')
    Journal = Model.get('account.payment.journal')
    Line = Model.get('account.move.line')
    Payment = Model.get('account.payment')

    usd, = Currency.find([('code', '=', 'USD')])
    journal = Journal(name='Manual', currency=usd, company=company,
        process_method='manual')
    journal.save()

    lines = Line.find([
            ('account.type.payable', '=', True),
            ('party', '!=', None),
            ('reconciliation', '=', None),
            ('payment_amount', '!=', 0),
            ])
    lines = random.sample(lines, len(lines) * 2 // 3)
    if not lines:
        return

    logger.info('Creating payments')

    pay_line = Wizard('account.move.line.pay', lines)
    pay_line.execute('next_')
    pay_line.form.journal = journal
    pay_line.execute('next_')

    payments = Payment.find([])
    payments = random.sample(payments, len(payments) * 2 // 3)

    for payment in payments:
        payment.click('approve')

    payments = random.sample(payments, len(payments) * 2 // 3)
    i = j = 0
    while i < len(payments):
        j = random.randint(1, 5)
        Wizard('account.payment.process', payments[i:i + j])
        i += j


def process_sales(config, modules):
    try:
        Opportunity = Model.get('sale.opportunity')
    except KeyError:
        Opportunity = None
    Sale = Model.get('sale.sale')

    if Sale.find([('state', '!=', 'draft')]):
        return

    logger.info('Processing sales')
    try:
        Agent = Model.get('commission.agent')
        agents = Agent.find([])
    except KeyError:
        Agent = None
    try:
        Carrier = Model.get('carrier')
        carriers = Carrier.find([])
    except KeyError:
        Carrier = None

    sales = Sale.find([('state', '=', 'draft')])
    sales = random.sample(sales, int(0.9 * len(sales)))
    for sale in sales:
        if (not sale.sale_date
                and Opportunity and isinstance(sale.origin, Opportunity)):
            sale.sale_date = sale.origin.end_date
        if sale.sale_date and sale.sale_date > datetime.date.today():
            sale.sale_date = datetime.date.today()
            continue
        if Agent:
            sale.agent = random.choice(agents)
        if 'sale_shipment_cost' in modules and not sale.carrier:
            sale.carrier = random.choice(carriers)

    if sales:
        Sale.click(sales, 'quote')

    cancel = random.sample(sales, int(0.2 * len(sales)))
    if cancel:
        Sale.click(cancel, 'cancel')

    # Only convert non-canceled sales
    to_confirm = list(set(sales) - set(cancel))
    sales = random.sample(to_confirm, int(0.8 * len(to_confirm)))
    if sales:
        Sale.click(sales, 'confirm')


def process_out_shipments(config):
    ShipmentOut = Model.get('stock.shipment.out')

    if ShipmentOut.find([('state', '=', 'done')]):
        return

    logger.info('Processing out shipments')

    try:
        Lot = Model.get('stock.lot')
        lots = defaultdict(list)
        for lot in Lot.find([]):
            lots[lot.product].append(lot)
    except KeyError:
        lots = {}

    to_finish = []
    for shipment in ShipmentOut.find([('state', '=', 'waiting')]):
        if lots:
            for move in shipment.inventory_moves:
                if move.product in lots:
                    move.lot = random.choice(lots[move.product])
        if shipment.click('assign_try'):
            to_finish.append(shipment)
        elif random.choice([True, False]):
            # Partial shipment
            if any(m.state == 'assigned' for m in shipment.inventory_moves):
                for move in shipment.inventory_moves:
                    if move.state == 'draft':
                        shipment.inventory_moves.remove(move)
                if shipment.click('assign_try'):
                    to_finish.append(shipment)

    today = datetime.date.today()
    for shipment in to_finish:
        date = shipment.planned_date
        shipment.effective_date = min(
            random_datetime(date - relativedelta(days=7), date),
            today)
    ShipmentOut.click(to_finish, 'pack')
    shipments = random.sample(to_finish, int(0.9 * len(to_finish)))
    if shipments:
        ShipmentOut.click(shipments, 'done')


def setup_project(config, activated, company):
    Work = Model.get('project.work')
    Party = Model.get('party.party')
    if Work.find([], limit=1):
        return
    customers = Party.find([], limit=10)
    logger.info('Creating projects')
    customer_projects = {
            'Cablear Marenostrum': ['Análisis', 'Diseño', 'Cablear'],
            'Copias de seguridad automáticas': [
                'Análisis',
                'Crear copias automáticas',
                'Verificar protocolo de seguridad'],
            'Página web': [
                'Diseño inicial',
                'Desarrollo lógica web',
                'Verificación y ajuste con el cliente'],
            'Tienda online': [
                'Diseño inicial',
                'Desarrollo lógica web',
                'Verificación y ajuste con el cliente'],
            }
    for name, task_names in customer_projects.items():
        project = Work(name=name, type='project', timesheet_available=False)
        project.party = random.choice(customers)
        for task_name in task_names:
            task = Work(name=task_name, type='task', timesheet_available=True)
            task.effort_duration = datetime.timedelta(
                days=random.randint(10, 30),
                hours=random.randint(1, 5))
            task.progress = random.randint(1, 100) // 5 * 5 / 100.
            project.children.append(task)
        project.save()


def process_purchases(config):
    PurchaseRequest = Model.get('purchase.request')
    Purchase = Model.get('purchase.purchase')
    today = datetime.date.today()

    if Purchase.find([]):
        return

    logger.info('Processing purchases')

    requests = []
    for request in PurchaseRequest.find([('party', '!=', None)]):
        if random.random() > 0.8:
            continue
        requests.append(request)

    if not requests:
        return
    Wizard('purchase.request.create_purchase', requests)

    to_quote = []
    purchases = Purchase.find([])
    for purchase in purchases:
        if random.random() > 0.9:
            continue
        purchase.purchase_date = random_datetime(
            today - relativedelta(months=6),
            today)
        to_quote.append(purchase)
    if to_quote:
        Purchase.click(to_quote, 'quote')
    purchases = random.sample(to_quote, int(0.5 * len(purchases)))
    if purchases:
        Purchase.click(purchases, 'confirm')


def process_invoices(config, company, type_, modules):
    Invoice = Model.get('account.invoice')
    try:
        PaymentType = Model.get('account.payment.type')
    except KeyError:
        PaymentType = None

    invoices = Invoice.find([
            ('type', '=', type_),
            ('state', '=', 'draft'),
            ])
    posted_invoices = Invoice.find([
            ('type', '=', type_),
            ('state', '=', 'posted'),
            ])
    if invoices:
        invoices = random.sample(invoices, int(0.9 * len(invoices)))
        # Avoid reposting with already posted invoices due to errors in date
        if invoices and not posted_invoices:
            logger.info('Posing invoices of type: %s', type_)
            to_post = []
            for invoice in invoices:
                # Skip invoices that can not be posted
                if PaymentType and (invoice.payment_type
                        and invoice.payment_type.account_bank != 'none'
                        and not invoice.bank_account):
                    continue
                if not invoice.invoice_date:
                    if invoice.sales:
                        date = invoice.sales[0].sale_date
                    elif invoice.purchases:
                        date = invoice.purchases[0].purchase_date
                    else:
                        date = datetime.date.today()
                    invoice.invoice_date = random_datetime(
                        date + relativedelta(days=1),
                        date + relativedelta(days=15),
                        )
                if (invoice.invoice_date
                        and invoice.invoice_date > datetime.date.today()):
                    continue
                if invoice.type == 'in' and not invoice.reference:
                    invoice.reference = ''.join(
                        random.choice(string.ascii_uppercase + string.digits)
                        for _ in range(random.randint(4, 8)))
                to_post.append(invoice)
            to_post = sorted(to_post, key=lambda a: a.invoice_date)
            Invoice.click(to_post, 'post')
    invoices = Invoice.find([
            ('type', '=', type_),
            ('state', '=', 'posted'),
            ])
    if invoices:
        PaymentMethod = Model.get('account.invoice.payment.method')
        methods = PaymentMethod.find([])
        if not methods:
            Journal = Model.get('account.journal')
            journal_cash, = Journal.find([('type', '=', 'cash')])
            payment_method = PaymentMethod()
            payment_method.name = 'Efectivo'
            payment_method.journal = journal_cash
            Account = Model.get('account.account')
            name = 'Caja, euros' if 'account_es' in modules else 'Main Cash'
            domain = [
                ('name', '=', name),
                ('company', '=', company.id),
                ]
            if trytond_version >= (5, 1):
                domain.append(('type', '!=', None))
            else:
                domain.append(('kind', '!=', 'view'))
            payment_method.credit_account, = Account.find(domain, limit=1)
            payment_method.debit_account, = Account.find(domain, limit=1)
            payment_method.save()
            methods = [payment_method]
        for invoice in invoices:
            due = invoice.amount_to_pay_today == invoice.total_amount
            cash = random.random() < 0.6
            # XXX: Manage invoice direct debit
            # XXX: Take in account sepa mandates
            if PaymentType:
                cash = random.random() < 0.8 and (not invoice.payment_type
                    or invoice.payment_type.account_bank == 'none')
            # Pay cash invoices
            if due and cash:
                pay = Wizard('account.invoice.pay', [invoice])
                pay.form.payment_method = random.choice(methods)
                pay.execute('choice')


def create_dunnings(config, date=None):
    if date is None:
        date = datetime.date.today()
    logger.info('Creating dunnings')
    create_dunning = Wizard('account.dunning.create')
    create_dunning.form.date = date
    create_dunning.execute('create_')


def create_commission_invoices(config):
    logger.info('Creating commission invoices')
    create_invoice = Wizard('commission.create_invoice')
    create_invoice.form.from_ = None
    create_invoice.form.to = None
    create_invoice.execute('create_')


def load_zips():
    Zip = Model.get('country.postal_code')
    Country = Model.get('country.country')
    Currency = Model.get('currency.currency')
    if not Country.find([], limit=1):
        from trytond.modules.country.scripts.import_countries import do_import
        do_import()
    if not Currency.find([], limit=1):
        from trytond.modules.currency.scripts.import_currencies import (
            do_import)
        do_import()
    if not Zip.find([], limit=1):
        from trytond.modules.country.scripts.import_postal_codes import (
            do_import)
        do_import(['es'])


def main(database, config_file=None):
    database_uri = os.environ.get('TRYTOND_DATABASE_URI')
    if database_uri and not database.startswith(database_uri):
        database = '%s%s' % (database_uri, database)
    config = pconfig.set_trytond(database, config_file=config_file)
    modules = get_active_modules()
    # Activate some kind of default modules
    if len(modules) <= 2:
        activate_modules()
        modules = get_active_modules()
    if 'bank' in modules:
        load_banks()
    if 'country' in modules:
        load_zips()
    if 'company' in modules:
        company_id = create_company(config)
        # set_tryton another time so context is updated
        config = pconfig.set_trytond(database, config_file=config_file)
        company = setup_company(config, company_id, modules)
        create_employees(config, company)
    with config.set_context(_skip_warnings=True):
        if 'account_invoice' in modules:
            create_payment_terms(config)
            create_recurring_invoices(config, company)
        if 'account_payment_type' in modules:
            create_payment_types(config, company)
        if 'account_payment_sepa' in modules:
            create_sepa_journals(config, company)
        if 'account_dunning' in modules:
            create_dunning_procedures(config)
        if 'product' in modules:
            create_product_categories(config)
            create_products(config, company, modules)
        if 'production' in modules:
            routings = []
            if 'production_routing' in modules:
                routings = setup_production_routing(config, modules, company)
            if 'production_work' in modules:
                setup_production_work(config, modules, company)
            setup_production(config, modules, routings)
        if 'stock_supply' in modules:
            create_order_points(config, modules)
        if 'product_price_list' in modules:
            create_price_lists(config)
        if 'party' in modules:
            create_parties(config, modules)
        if ('project' in modules
                or 'project_revenue' in modules
                or 'project_invoice' in modules):
            setup_project(config, modules, company)
        if 'purchase' in modules:
            create_product_supplier(config)
        if 'account_asset' in modules:
            create_assets(config, company, modules)
        if 'carrier' in modules:
            create_carriers(config)
        if 'commission' in modules:
            create_agents(config, company)
        if 'stock_lot' in modules:
            create_lots(config, modules)
        if 'stock' in modules:
            create_inventory(config)
        if 'sale_opportunity' in modules:
            create_opportunities(config, modules)
            process_sales(config, modules)
        elif 'sale' in modules:
            create_sales(config, modules)
            process_sales(config, modules)
        if 'sale' in modules:
            process_out_shipments(config)
            process_invoices(config, company, 'out', modules)
        if 'account_payment' in modules:
            create_payments(config, company)
        # TODO: Generate common account moves? I.E: Employee Salary Moves
        # TODO: Generate bank statements?
        if 'account_dunning' in modules:
            create_dunnings(config)
        if 'commission' in modules:
            create_commission_invoices(config)
        if 'production' in modules:
            create_productions(config, modules)
        if 'stock_supply' in modules:
            supply_stock(config, modules)
            process_purchases(config)
        elif 'purchase' in modules:
            create_purchases(config, modules)
        if 'purchase' in modules:
            process_purchase_shipments(config, modules)
            process_invoices(config, company, 'in', modules)

    ConfigWizardItem = Model.get('ir.module.config_wizard.item')
    items = []
    for item in ConfigWizardItem.find([('state', '!=', 'done')]):
        item.state = 'done'
        items.append(item)
    ConfigWizardItem.save(items)
