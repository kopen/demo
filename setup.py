#!/usr/bin/env python3
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from setuptools import setup


setup(name='kopen_demo',
    version='0.1',
    description='A script for creating tryton demos databases',
    author='Sergi Almacellas Abellana',
    author_email='sergi@kopen.es',
    url='http://www.kopen.es/',
    keywords='',
    packages=['kopen_demo'],
    package_dir={'kopen_demo': '.'},
    package_data={
        'kopen_demo': ['demo_data/*'],
        },
    scripts=['load_tryton_demo'],
    requires=['xmltodict', 'faker'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: Catalan',
        'Natural Language :: English',
        'Natural Language :: Spanish',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Office/Business',
        ],
    license='GPL-3',
    install_requires=['proteus', 'xmltodict', 'faker', 'python-stdnum'],
    zip_safe=False,
    )
